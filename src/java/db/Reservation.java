/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
/**
 *
 * @author radic
 */
@Entity  
@Table(name = "reservation")
public class Reservation implements Serializable{
    
    
    public static final int USED_CHOOSE = 0;
    public static final int USED_NO = 1;
    public static final int USED_YES = 2;
    
@Id  
@GeneratedValue(strategy=GenerationType.AUTO)        
@Column(name = "id")  
private int id; 

@ManyToOne
@JoinColumn(name = "teacher")
private Teacher teacher;

@Column(name= "date")
private Date date;

@Column(name= "startHour")
private int startHour;

@Column(name= "startMinute")
private int startMinute;

@Column(name= "endHour")
private int endHour;

@Column(name= "endMinute")
private int endMinute;

@Column(name="used")
private int used;

@Column(name="year")
private int year;

@Column(name="type")
private String type;

@Column(name="description")
private String description;

@Transient
private List<ReservationAuditorium> auditoriums;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the teacher
     */
    public Teacher getTeacher() {
        return teacher;
    }

    /**
     * @param teacher the teacher to set
     */
    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }


    /**
     * @return the date
     */
    public Date getDate() {
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
        this.date.setHours(0);
        this.date.setMinutes(0);
        this.date.setSeconds(0);
    }

    /**
     * @return the used
     */
    public int getUsed() {
        return used;
    }

    /**
     * @param used the used to set
     */
    public void setUsed(int used) {
        this.used = used;
    }

    /**
     * @return the startHour
     */
    public int getStartHour() {
        return startHour;
    }

    /**
     * @param startHour the startHour to set
     */
    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    /**
     * @return the startMinute
     */
    public int getStartMinute() {
        return startMinute;
    }

    /**
     * @param startMinute the startMinute to set
     */
    public void setStartMinute(int startMinute) {
        this.startMinute = startMinute;
    }

    /**
     * @return the endHour
     */
    public int getEndHour() {
        return endHour;
    }

    /**
     * @param endHour the endHour to set
     */
    public void setEndHour(int endHour) {
        this.endHour = endHour;
    }

    /**
     * @return the endMinute
     */
    public int getEndMinute() {
        return endMinute;
    }

    /**
     * @param endMinute the endMinute to set
     */
    public void setEndMinute(int endMinute) {
        this.endMinute = endMinute;
    }

    /**
     * @return the auditoriums
     */
    public List<ReservationAuditorium> getAuditoriums() {
        return auditoriums;
    }

    /**
     * @param auditoriums the auditoriums to set
     */
    public void setAuditoriums(List<ReservationAuditorium> auditoriums) {
        this.auditoriums = auditoriums;
    }

    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
