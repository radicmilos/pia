/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 *
 * @author radic
 */
@Entity  
@Table(name = "admin")
@PrimaryKeyJoinColumn(name="id")  
public class Admin extends User {
    
}
