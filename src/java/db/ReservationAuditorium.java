/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author radic
 */
@Entity  
@Table(name = "reservation_auditorium")
public class ReservationAuditorium implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)        
    @Column(name = "id")  
    private Long id;
    
    @ManyToOne
    @JoinColumn(name = "reservation")
    private Reservation reservation;
    
    @ManyToOne
    @JoinColumn(name = "auditorium")
    private Auditorium auditorium;
    
    @Transient
    private String auditorium_name;

    /**
     * @return the reservation
     */
    public Reservation getReservation() {
        return reservation;
    }

    /**
     * @param reservation the reservation to set
     */
    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    /**
     * @return the auditorium
     */
    public Auditorium getAuditorium() {
        return auditorium;
    }

    /**
     * @param auditorium the auditorium to set
     */
    public void setAuditorium(Auditorium auditorium) {
        this.auditorium = auditorium;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the auditorium_name
     */
    public String getAuditorium_name() {
        return auditorium_name;
    }

    /**
     * @param auditorium_name the auditorium_name to set
     */
    public void setAuditorium_name(String auditorium_name) {
        this.auditorium_name = auditorium_name;
    }
}
