/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import javax.persistence.*;
/**
 *
 * @author radic
 */
@Entity  
@Table(name = "student")
@PrimaryKeyJoinColumn(name="id")  
public class Student extends User {
    @Column(name="year")
    private String year;
    
    @Column(name="field")
    private String field;

    /**
     * @return the year
     */
    public String getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * @param type the type to set
     */
    public void setField(String field) {
        this.field = field;
    }

    /**
     * @return the type
     */
    public String getField() {
        return field;
    }
    
}
