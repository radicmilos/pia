/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author radic
 */
@Entity  
@Table(name = "auditorium")  
public class Auditorium implements Serializable{
@Id  
@GeneratedValue(strategy=GenerationType.AUTO)        
@Column(name = "id")  
private int id;

@Column(name="name")
private String name;

@Column(name="capacity")
private int capacity = 30;

@Column(name="whiteboard")
private int whiteboard;

@Column(name="projector")
private int projector;

@Column(name="master_comp")
private int master_comp;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the capacity
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * @param capacity the capacity to set
     */
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    /**
     * @return the whiteboard
     */
    public int getWhiteboard() {
        return whiteboard;
    }

    /**
     * @param whiteboard the whiteboard to set
     */
    public void setWhiteboard(int whiteboard) {
        this.whiteboard = whiteboard;
    }

    /**
     * @return the projector
     */
    public int getProjector() {
        return projector;
    }

    /**
     * @param projector the projector to set
     */
    public void setProjector(int projector) {
        this.projector = projector;
    }

    /**
     * @return the master_comp
     */
    public int getMaster_comp() {
        return master_comp;
    }

    /**
     * @param master_comp the master_comp to set
     */
    public void setMaster_comp(int master_comp) {
        this.master_comp = master_comp;
    }
}