/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import javax.persistence.*;

/**
 *
 * @author radic
 */
@Entity  
@Table(name = "teacher")  
@PrimaryKeyJoinColumn(name="id")  
public class Teacher extends User{
    @Column(name="office")
    private String office;
    
    @Column(name="title")
    private String title;

    /**
     * @return the office
     */
    public String getOffice() {
        return office;
    }

    /**
     * @param office the office to set
     */
    public void setOffice(String office) {
        this.office = office;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
    
}
