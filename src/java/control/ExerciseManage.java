/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import db.Auditorium;
import db.DbFactory;
import db.Reservation;
import db.ReservationAuditorium;
import db.ReservationStudent;
import db.Student;
import db.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author radic
 */
@ManagedBean
@ViewScoped
public class ExerciseManage implements Serializable{

    private Integer count[];
    
    /**
     * @return the exercises
     */
    public ExerciseCollection[] getExercises() {
        return exercises;
    }

    /**
     * @param exercises the exercises to set
     */
    public void setExercises(ExerciseCollection[] exercises) {
        this.exercises = exercises;
    }

    /**
     * @return the count
     */
    public Integer[] getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Integer[] count) {
        this.count = count;
    }

    public void change(Exercise e)
    {
        if (e.isIsMine())
        {
                    Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(e.getR());
        session.getTransaction().commit();
        session.close();
            e.setIsMine(false);
            
        }
        else
        {
            ReservationStudent resStud = new ReservationStudent();
            resStud.setReservation(e.getReservation());
            resStud.setStudent((Student)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user"));
            e.setR(resStud);
         Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(resStud);
        session.getTransaction().commit();
        session.close();
            e.setIsMine(true);
        }
    }
    public static class Exercise
    {
        private String auditoriums;
        private Reservation reservation;
        private boolean isMine;
        private ReservationStudent r;
        
        private Exercise(Reservation res, String auditoriums, boolean isMine, ReservationStudent r) {
             this.reservation = res;
             this.auditoriums = auditoriums;
             this.isMine = isMine;
             this.r =r;
        }

        public String getTime()
        {
            String time = "";
            time += reservation.getStartHour() + ":" + reservation.getStartMinute();
            time += "-";
            time += reservation.getEndHour() + ":" + reservation.getEndMinute();
            return time;
        }
        
        /**
         * @return the auditoriums
         */
        public String getAuditoriums() {
            return auditoriums;
        }

        /**
         * @param auditoriums the auditoriums to set
         */
        public void setAuditoriums(String auditoriums) {
            this.auditoriums = auditoriums;
        }

        /**
         * @return the reservation
         */
        public Reservation getReservation() {
            return reservation;
        }

        /**
         * @param reservation the reservation to set
         */
        public void setReservation(Reservation reservation) {
            this.reservation = reservation;
        }

        /**
         * @return the isMine
         */
        public boolean isIsMine() {
            return isMine;
        }

        /**
         * @param isMine the isMine to set
         */
        public void setIsMine(boolean isMine) {
            this.isMine = isMine;
        }

        /**
         * @return the r
         */
        public ReservationStudent getR() {
            return r;
        }

        /**
         * @param r the r to set
         */
        public void setR(ReservationStudent r) {
            this.r = r;
        }
    }
    public static class ExerciseCollection
    {
        private List<Exercise> collection = new ArrayList<Exercise> ();

        /**
         * @return the collection
         */
        public List<Exercise> getCollection() {
            return collection;
        }

        /**
         * @param collection the collection to set
         */
        public void setCollection(List<Exercise> collection) {
            this.collection = collection;
        }
    }
    private ExerciseCollection exercises[] = new ExerciseCollection [7];
    {
        for (int i = 0; i < exercises.length; ++i)
            exercises[i] = new ExerciseCollection();
    }
    /**
     * Creates a new instance of ExerciseManage
     */
    
    public void getExercises(Date date1, Date date2)
    {
        int sum[] = new int[7];
        User student = (User)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        Query q = session.createQuery("FROM Reservation order by startHour, startMinute, endHour, endMinute asc");
        List<Reservation> reservs = q.list();
        
        for (Reservation r : reservs)
        {
            if (student.getType() == User.STUDENT_TYPE)
            {
                Student s = (Student)student;
                if (r.getYear() != Integer.valueOf(s.getYear()))
                    continue;
                if (!r.getType().equals(s.getField()))
                    continue;
            }
            if (r.getDate().getDate() < date1.getDate() || r.getDate().getDate() > date2.getDate())
                continue;
            q = session.createQuery("From ReservationStudent where student = " + student.getId() + " and reservation = " + r.getId());
            ReservationStudent resStud = null;
            boolean isMine = false;
            if (q.list().size() > 0)
            {
                isMine = true;
                resStud = (ReservationStudent) q.list().get(0);
            }
            if (student.getType() == User.TEACHER_TYPE)
            {
                if (r.getTeacher().getId() == student.getId())
                    isMine = true;
            }
            q = session.createQuery("From ReservationAuditorium where reservation = " + r.getId());
            List<ReservationAuditorium> auds = q.list();
            String audName = "";
            for (ReservationAuditorium a : auds)
            {
             if (student.getType() == User.TEACHER_TYPE)
            {
                Auditorium aud = (Auditorium)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("auditorium");
                if (aud.getId() != a.getAuditorium().getId())
                    continue;
            }
                audName += a.getAuditorium().getName() + " ";
            }
            if (audName.equals(""))
                continue;
            long diff = r.getDate().getDate() - date1.getDate();
            long dayDiff = diff;
            //long dayDiff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            getExercises()[(int)dayDiff].getCollection().add(new Exercise(r, audName, isMine, resStud));
            sum[(int)dayDiff]++;
        }
        session.getTransaction().commit();
        session.close();
        int max = sum[0];
        for (int i = 1; i < 7; ++i)
            if (sum[i] > max)
                max = sum[i];
        count = new Integer[max];
        for (int i = 0; i < max; ++i)
            count[i] = new Integer(i);
    }
    
    public void loadExercises()
    {
        Date d = new Date();
        d.setHours(0);
        d.setMinutes(0);
        d.setSeconds(0);
        long DAY_IN_MS = 1000 * 60 * 60 * 24;
        Date d1 = new Date(d.getTime() - (DAY_IN_MS) * (d.getDay() - 1));
        Date d2 = new Date(d.getTime() + (DAY_IN_MS) * (7 - d.getDay()));
        getExercises(d1, d2);
    }
    
    public ExerciseManage() {
    }
    
    
    
}
