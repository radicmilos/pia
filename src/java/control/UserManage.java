/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import com.sun.faces.config.WebConfiguration;
import db.Admin;
import db.DbFactory;
import db.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.view.facelets.FaceletContext;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author radic
 */
@ManagedBean
@ViewScoped
public class UserManage implements Serializable{

    private List<User> users;
    /**
     * Creates a new instance of UserManage
     */
    public UserManage() {
    }
    
    public void showAllUsers()
    {
        
       User admin = (Admin)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
       Session session = DbFactory.getSessionFactory().openSession();
       session.beginTransaction();
       Query q = session.createQuery("From User where id !=" + admin.getId() + " and approved =" + User.APPROVED_YES);
       users = q.list();
       session.close();
    }
    
    public void showUsers()
    {
       Session session = DbFactory.getSessionFactory().openSession();
       session.beginTransaction();
       Query q = session.createQuery("From User where approved=0");
       users = q.list();
       session.close();
    }
    
    public void enable(User user)
    {
        user.setApproved(User.APPROVED_YES);
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(user);
        session.getTransaction().commit();
        session.close();
        users.remove(user);
    }
    
    public String disable(User user)
    {
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        if (user.getType() != User.STUDENT_TYPE)
        {
            user.setApproved(User.APPROVED_NO);
            session.update(user);
        }
        else
        {
            session.createQuery("Delete ReservationStudent where student = " + user.getId());
            session.delete(user);
        }
        session.getTransaction().commit();
        session.close();      
        users.remove(user);
        return "user_overview.xhtml?faces-redirect=true";
    }

    /**
     * @return the users
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * @param users the users to set
     */
    public void setUsers(List<User> users) {
        this.users = users;
    }
    
}
