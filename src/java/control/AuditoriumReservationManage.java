/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import db.Auditorium;
import db.DbFactory;
import db.Reservation;
import db.ReservationAuditorium;
import db.Teacher;
import db.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author radic
 */
@ManagedBean
@ViewScoped

public class AuditoriumReservationManage implements Serializable{

    private Date date;
    private String capacity;
    private String startHour;
    private String startMinute;
    private String durationHour;
    private String durationMinute;
    private Boolean whiteboard;
    private Boolean projector;
    private Boolean master_comp;
    private Boolean multiple; 
    private String auditoriumName;
    private String description;
    private String year;
    private String type;
    
    private User thisUser;
    private User reservUser;
    
    private int auditoriumID;
    private List<Reservation> reservations;
    
    private List<ReservationAuditorium> resAuds;
    private List<AuditoriumCollection> possibleAuditoriums;
    
    private Reservation reservation;
    private Reservation reservation_made;
    private Auditorium auditorium;
    /**
     * Creates a new instance of AuditoriumReservationManage
     */
    
    public AuditoriumReservationManage() {
    }

    
    public void thisUserShow(User user)
    {
        this.thisUser = user;
        reservations = null;
    }
    public Reservation getReservation()
    {
        return reservation;
    }
    public void auditoriumsForReservation(Reservation reservation)
    {
        this.reservation = reservation;
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        Query q = session.createQuery("FROM ReservationAuditorium where reservation = " + reservation.getId());
        reservation.setAuditoriums(q.list());
        session.close();
        
    }
    
    public String checkAndReserve()
    {
        Auditorium ad = (Auditorium)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("auditorium");
        capacity = "1";
        setWhiteboard((Boolean) false);
        projector = false;
        master_comp = false;
        multiple = false;
        searchFree();
        for (AuditoriumCollection o : this.possibleAuditoriums)
        {
            if (o.auditoriums.get(0).getId() == ad.getId())
            {
                saveReservation(o);
                return "home.xhtml?faces-redirect=true";
            }
        }
        FacesContext.getCurrentInstance().addMessage("jsf_message", new FacesMessage("Nije moguca rezervacija"));
        return null;
    }
    
    public User getReservUser()
    {
        return reservUser;
    }
    
    public void getReservationsForUser(User user)
    {
//        reservUser = user;
        thisUser = null;
        reservUser = user;
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction(); 
        Query q = session.createQuery("FROM Reservation where teacher = " + user.getId() + " order by date,startHour,startMinute,endHour,endMinute asc");
        reservations = q.list();
        if (reservations == null)
            reservations = new ArrayList<Reservation> ();
        session.close();
    }
 /*public void showAvailable()
    {
        int hourStartVal;
        int minStartVal;
        int hourEndVal;
        int minEndVal;
        List<Reservation> reservations;
        try{
            hourStartVal = Integer.parseInt(getStartHour());
            minStartVal = Integer.parseInt(getStartMinute());
            hourEndVal = hourStartVal + Integer.parseInt(getDurationHour());
            minEndVal = minStartVal + Integer.parseInt(getDurationMinute());
        }
        catch(Exception e) {}
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        Query q = session.createQuery("FROM Auditorium");
        setAvailable((List<Auditorium>) q.list());
        q = session.createQuery("FROM Reservation");
        reservations = q.list();
        session.getTransaction().commit();
        session.close();
    }*/
   
    public void removeReservationAuditorium(ReservationAuditorium ra)
    {
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(ra);
        reservation.getAuditoriums().remove(ra);
        if (reservation.getAuditoriums().isEmpty())
        {
            session.delete(reservation);
            reservations.remove(reservation);
            reservation = null;
        }
        session.getTransaction().commit();
        session.close();       
    }
    
    public void studentShowReservations()
    {
resAuds = new ArrayList<ReservationAuditorium> ();
        Session session = DbFactory.getSessionFactory().openSession();
        Query q;
        List<Reservation> results;
        List<ReservationAuditorium> results2;
        session.beginTransaction();
        q = session.createQuery("FROM Reservation WHERE date=:d");
        q.setParameter("d", new java.sql.Date(date.getTime()));
        results = q.list();
        session.close();
        session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        q = session.createQuery("From ReservationAuditorium");
        results2 = q.list();
        for (Reservation r : results)
        {
            for (ReservationAuditorium ra : results2)
            {
                if (!ra.getAuditorium().getName().equals(auditoriumName))
                    continue;
                if (ra.getReservation().getId() == r.getId())
                {
                    if (resAuds == null)
                        resAuds = new ArrayList<ReservationAuditorium> ();
                    resAuds.add(ra);
                }
            }
        }
    }
    
    public void removeReservation(Reservation reservation)
    {
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        Query q = session.createQuery("FROM ReservationAuditorium where reservation = " + reservation.getId());
        reservation.setAuditoriums(q.list());
        for (ReservationAuditorium ra : reservation.getAuditoriums())
        {
            session.delete(ra);
        }
        session.getTransaction().commit();
        session.close();
        session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(reservation);
        session.getTransaction().commit();
        session.close();
        reservations.remove(reservation);
        this.reservation = null;
    }
    
    public void myCurrentReservations()
    {
        Date date = new Date();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        User currentUser = (User)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
        int id = currentUser.getId();
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        Query q = session.createQuery("FROM Reservation where teacher = " + id);

        reservations = q.list();
        for (int i = 0; i < reservations.size() ; ++i)
        {
            if (reservations.get(i).getDate().before(date))
            {
                reservations.remove(i);
                --i;
            }
        }
        session.close();
    }
    
        public void myOldReservations()
    {
        Date date = new Date();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        User currentUser = (User)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
        int id = currentUser.getId();
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        Query q = session.createQuery("FROM Reservation where teacher = " + id);
        reservations = q.list();
        for (int i = 0; i < reservations.size(); ++i)
        {
            if (!reservations.get(i).getDate().before(date))
            {
                reservations.remove(i);
                --i;
            }
        }
        session.close();
    }
    public void used(Reservation reservation, boolean flag)
    {
        if (flag)
            reservation.setUsed(Reservation.USED_YES);
        else
            reservation.setUsed(Reservation.USED_NO);
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(reservation);
        session.getTransaction().commit();
        session.close();
    }
    
    public void showAllReservations()
    {
         Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        Query q = session.createQuery("From ReservationAuditorium");    
        List<ReservationAuditorium> possible = q.list();
        for (ReservationAuditorium ra : possible)
        {
            if (ra.getReservation().getDate().equals(date))
                this.resAuds.add(ra);
        }    
        session.close();
    }
    public void showReservations()
    {
        resAuds = new ArrayList<ReservationAuditorium> ();
        String audNames[] = null;
        if (auditoriumName != null && !auditoriumName.equals(""))
            audNames = auditoriumName.split(",");
        Session session = DbFactory.getSessionFactory().openSession();
        Query q;
        List<Reservation> results;
        List<ReservationAuditorium> results2;
        session.beginTransaction();
        q = session.createQuery("FROM Reservation WHERE date=:d order by startHour, startMinute, endHour, endMinute asc");
        q.setParameter("d", new java.sql.Date(date.getTime()));
        results = q.list();
        session.close();
        session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        q = session.createQuery("From ReservationAuditorium");
        results2 = q.list();
        for (Reservation r : results)
        {
            for (ReservationAuditorium ra : results2)
            {
                if (ra.getReservation().getId() == r.getId())
                {
                    if (audNames != null)
                    {
                        boolean toSkip = true;
                        for (String s : audNames)
                        {
                            if (ra.getAuditorium().getName().equals(s))
                            {
                                toSkip = false;
                                break;
                            }
                        }
                        if (toSkip)
                            continue;
                    }
                    if (resAuds == null)
                        resAuds = new ArrayList<ReservationAuditorium> ();
                    resAuds.add(ra);
                }
            }
        }
    }
    
    public String saveReservation(AuditoriumCollection ac)
    {
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        reservation_made.setDescription(description);
        reservation_made.setType(type);
        reservation_made.setYear(Integer.parseInt(year));
        session.save(reservation_made);
        session.flush();
        session.getTransaction().commit();
        session.clear();
        session.beginTransaction();
        for (Auditorium a : ac.getAuditoriums())
        {
            ReservationAuditorium ra = new ReservationAuditorium();
            ra.setAuditorium(a);
            ra.setReservation(reservation_made);
            session.save(ra);
        }
        session.getTransaction().commit();
        session.close();     
        return "add_reservation.xhtml?faces-redirect=true";
    }
    
    public void generateCapacityOk(List<Auditorium> auditoriums, int capacity, List<Auditorium> oldCollection, int index)
    {
        if (!multiple)
        {
            for (Auditorium a : auditoriums)
            {
                List<Auditorium> temp = new ArrayList<Auditorium> ();
                temp.add(a);
                makePossibleFree(temp, capacity);
            }
            return;
        }
        
        if (index >= auditoriums.size()) 
            return;
        oldCollection.add(auditoriums.get(index));
        if (!makePossibleFree(oldCollection, capacity))
            generateCapacityOk(auditoriums, capacity, oldCollection, index + 1);
        oldCollection.remove(auditoriums.get(index));
        generateCapacityOk(auditoriums, capacity, oldCollection, index + 1);     
    }
    public boolean makePossibleFree(List<Auditorium> allAuditoriums, int capacity)
    {
        int current = 0;
        String name="";
        for (Auditorium a : allAuditoriums)
        {
            current += a.getCapacity();
            name += a.getName() + " ";
        }
        if (current < capacity)
            return false;
        for (Auditorium a : allAuditoriums)
        {
            if (current - a.getCapacity() >= capacity)
                return true;
        }
        
        AuditoriumCollection ac = new AuditoriumCollection();
        ArrayList<Auditorium> auds = new ArrayList<Auditorium> ();
        for (Auditorium a : allAuditoriums)
        {
            auds.add(a);
        }
        ac.setAuditoriums(auds);
        ac.setCapacity(current);
        ac.setName(name);
        possibleAuditoriums.add(ac);
        return true;
    }
    
    public void searchFree()
    {
        possibleAuditoriums = new ArrayList<AuditoriumCollection> ();
        reservation_made = new Reservation();
        reservation_made.setDate(date);
        User user = (User)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
        reservation_made.setTeacher((Teacher)user);
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        String queryString = "FROM Auditorium where whiteboard>=:w and projector>= :p and master_comp>= :m"; 
        Query q = session.createQuery(queryString);
        q.setParameter("w", getWhiteboard()?1:0);
        q.setParameter("p", projector?1:0);
        q.setParameter("m", master_comp?1:0);
        List<Auditorium> allAuditoriums = q.list();
        q = session.createQuery("From ReservationAuditorium");
        List<ReservationAuditorium> res = q.list();
        int startN = Integer.parseInt(startHour) * 60 + Integer.parseInt(startMinute);
        int endN = startN + Integer.parseInt(durationHour) * 60 + Integer.parseInt(durationMinute);
        reservation_made.setStartHour(Integer.parseInt(startHour));
        reservation_made.setStartMinute(Integer.parseInt(startMinute));
        reservation_made.setEndHour(endN / 60);
        reservation_made.setEndMinute(endN % 60);
        for (ReservationAuditorium rx : res)
        {
            Reservation r= rx.getReservation();
            int startR = r.getStartHour() * 60 + r.getStartMinute();
            int endR = startR + r.getEndHour() * 60 + r.getEndMinute();
            
            if (r.getDate().getTime() == date.getTime() && ((startR >= startN && startR <= endN) || (endR >= startN && endR <=endN) || (startR <= startN && endR >= endN)))
            {
                 allAuditoriums.remove(rx.getAuditorium());
            }
        }
        generateCapacityOk(allAuditoriums, Integer.valueOf(capacity), new ArrayList<Auditorium> (), 0);
        session.close();
    }
    
    public void loadReservationInfo(Reservation r)
    {
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        Query q = session.createQuery("From ReservationAuditorium where reservation=" + r.getId());
        r.setAuditoriums(q.list());
        session.close();
    }
    
    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the auditoriumID
     */
    public int getAuditoriumID() {
        return auditoriumID;
    }

    /**
     * @param auditoriumID the auditoriumID to set
     */
    public void setAuditoriumID(int auditoriumID) {
        this.auditoriumID = auditoriumID;
    }

    /**
     * @return the reservations
     */
    public List<Reservation> getReservations() {
        return reservations;
    }

    /**
     * @param reservations the reservations to set
     */
    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    /**
     * @return the possibleAuditoriums
     */
    public List<AuditoriumCollection> getPossibleAuditoriums() {
        return possibleAuditoriums;
    }

    /**
     * @param possibleAuditoriums the possibleAuditoriums to set
     */
    public void setPossibleAuditoriums(List<AuditoriumCollection> possibleAuditoriums) {
        this.possibleAuditoriums = possibleAuditoriums    ;
                }

    /**
     * @return the capacity
     */
    public String getCapacity() {
        return capacity;
    }

    /**
     * @param capacity the capacity to set
     */
    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    /**
     * @return the resAuds
     */
    public List<ReservationAuditorium> getResAuds() {
        return resAuds;
    }

    /**
     * @param resAuds the resAuds to set
     */
    public void setResAuds(List<ReservationAuditorium> resAuds) {
        this.resAuds = resAuds;
    }

    /**
     * @return the startHour
     */
    public String getStartHour() {
        return startHour;
    }

    /**
     * @param startHour the startHour to set
     */
    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    /**
     * @return the startMinute
     */
    public String getStartMinute() {
        return startMinute;
    }

    /**
     * @param startMinute the startMinute to set
     */
    public void setStartMinute(String startMinute) {
        this.startMinute = startMinute;
    }

    /**
     * @return the durationHour
     */
    public String getDurationHour() {
        return durationHour;
    }

    /**
     * @param durationHour the durationHour to set
     */
    public void setDurationHour(String durationHour) {
        this.durationHour = durationHour;
    }

    /**
     * @return the durationMinute
     */
    public String getDurationMinute() {
        return durationMinute;
    }

    /**
     * @param durationMinute the durationMinute to set
     */
    public void setDurationMinute(String durationMinute) {
        this.durationMinute = durationMinute;
    }

    /**
     * @return the whiteboard
     */
    public Boolean getWhiteboard() {
        return whiteboard;
    }

    /**
     * @param whiteboard the whiteboard to set
     */
    public void setWhiteboard(Boolean whiteboard) {
        this.whiteboard = whiteboard;
    }

    /**
     * @return the projector
     */
    public Boolean getProjector() {
        return projector;
    }

    /**
     * @param projector the projector to set
     */
    public void setProjector(Boolean projector) {
        this.projector = projector;
    }

    /**
     * @return the master_comp
     */
    public Boolean getMaster_comp() {
        return master_comp;
    }

    /**
     * @param master_comp the master_comp to set
     */
    public void setMaster_comp(Boolean master_comp) {
        this.master_comp = master_comp;
    }

    /**
     * @return the multiple
     */
    public Boolean getMultiple() {
        return multiple;
    }

    /**
     * @param multiple the multiple to set
     */
    public void setMultiple(Boolean multiple) {
        this.multiple = multiple;
    }

    /**
     * @return the auditoriumName
     */
    public String getAuditoriumName() {
        return auditoriumName;
    }

    /**
     * @param auditoriumName the auditoriumName to set
     */
    public void setAuditoriumName(String auditoriumName) {
        this.auditoriumName = auditoriumName;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the year
     */
    public String getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the thisUser
     */
    public User getThisUser() {
        return thisUser;
    }

    /**
     * @param thisUser the thisUser to set
     */
    public void setThisUser(User thisUser) {
        this.thisUser = thisUser;
    }
    
    public static class AuditoriumCollection
    {
        private List<Auditorium> auditoriums;
        private String name;
        private int capacity;

        /**
         * @return the auditoriums
         */
        public List<Auditorium> getAuditoriums() {
            return auditoriums;
        }

        /**
         * @param auditoriums the auditoriums to set
         */
        public void setAuditoriums(List<Auditorium> auditoriums) {
            this.auditoriums = auditoriums;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the capacity
         */
        public int getCapacity() {
            return capacity;
        }

        /**
         * @param capacity the capacity to set
         */
        public void setCapacity(int capacity) {
            this.capacity = capacity;
        }
    }
}
