/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import db.Auditorium;
import db.DbFactory;
import db.Reservation;
import db.ReservationAuditorium;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author radic
 */
@ManagedBean
@ViewScoped
public class AuditoriumManage implements Serializable{
    
    private Auditorium auditorium = new Auditorium();
    
    
    private boolean canRemove;
    private boolean toRemove;
    private String message;
    private String message2;
    
    private String capacity;
    private boolean whiteboard;
    private boolean projector;
    private boolean master_comp;
    
    private Date date;

    
    private List<Auditorium> available = new ArrayList<Auditorium> ();
    private Auditorium auditorium_show;
    

    /**
     * Creates a new instance of AuditoriumManage
     */
    public AuditoriumManage() {
    }
    
    
    public String toReservations(Auditorium auditorium)
    {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("auditorium", auditorium);
        return "student_week.xhtml?faces-redirect=true";
    }
    
    public boolean checkPossible(Auditorium a)
    {
        Date date = new Date();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        Session session = DbFactory.getSessionFactory().openSession();
        Query q = session.createQuery("From ReservationAuditorium where auditorium = " + a.getId());
        List<ReservationAuditorium> resAuds = q.list();
        boolean isOk = true;
        for (ReservationAuditorium ra : resAuds)
        {
            if (!ra.getReservation().getDate().before(date))
            {
                isOk = false;
                break;
            }
        }
        session.close();
        if (!isOk)
        {
            return false;
        }
        else
        {
             return true;
        }
    }
    public void tryRemove(Auditorium auditorium)
    {
        auditorium_show = null;
        setToRemove(true);
        setCanRemove(checkPossible(auditorium));
        if (isCanRemove())
        {
            setMessage("Da li ste sigurni");
        }
        else
        {
            setMessage("Nije moguce trenutno");
        }
        this.auditorium = auditorium;
    }
    public Auditorium getAuditorium()
    {
        return auditorium;
    }
    
    public String addAuditorium()
    {
        auditorium.setCapacity(Integer.valueOf(capacity));
        auditorium.setWhiteboard(whiteboard?1:0);
        auditorium.setProjector(projector?1:0);
        auditorium.setMaster_comp(master_comp?1:0);
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(auditorium);
        session.getTransaction().commit();
        session.close();
        return "home.xhtml?faces-redirect=true";
    }

    public void removeAuditorium(boolean status)
    {
        if (status)
        {
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        session.createQuery("delete ReservationAuditorium where auditorium=" + auditorium.getId());
        session.getTransaction().commit();
        session.close();
        session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(auditorium);
        session.getTransaction().commit();
        session.close();
        available.remove(auditorium);
        }
        setToRemove(false);
        message = null;
    }
    
    public void getAuditorium(int id)
    {
    }
    
    public void showAuditoriums()
    {
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        Query q = session.createQuery("FROM Auditorium");
        setAvailable((List<Auditorium>) q.list());
        session.close();
    }
    
    public void showAuditoriumDescription(AjaxBehaviorEvent event)
    {
        auditorium_show = (Auditorium)event.getComponent().getAttributes().get("auditorium_show");
        master_comp = 0 != auditorium_show.getMaster_comp();
        projector = 0 != auditorium_show.getProjector();
        whiteboard = 0 != auditorium_show.getWhiteboard();
        toRemove = false;
        message2 = null;
    }
    
    
    public void updateAuditorium(boolean toSave)
    {
        if (toSave)
        {
        Date date = new Date();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        auditorium_show.setMaster_comp(master_comp?1:0);
        auditorium_show.setProjector(projector?1:0);
        auditorium_show.setWhiteboard(whiteboard?1:0);
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        Query q = session.createQuery("From ReservationAuditorium where auditorium = " + auditorium_show.getId());
        List<ReservationAuditorium> resAuds = q.list();
        boolean isOk = true;
        for (ReservationAuditorium ra : resAuds)
        {
            if (!ra.getReservation().getDate().before(date))
            {
                isOk = false;
                break;
            }
        }
        if (!isOk)
        {
            message2 = "Postoje tekuce rezervacije za ovu salu";
            return;
        }
        else
        {
           for (ReservationAuditorium ra : resAuds)
           {
               session.delete(ra);
           }
           session.flush();
           session.getTransaction().commit();
           session.clear();
        }
        session.beginTransaction();
        session.update(auditorium_show);
        session.getTransaction().commit();
        session.close();
        }
        setMessage2(null);
        auditorium_show = null;
    }
    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the startHour
     */
    /**
     * @return the available
     */
    public List<Auditorium> getAvailable() {
        return available;
    }

    /**
     * @param available the available to set
     */
    public void setAvailable(List<Auditorium> available) {
        this.available = available;
    }

    /**
     * @return the auditorium_show
     */
    public Auditorium getAuditorium_show() {
        return auditorium_show;
    }

    /**
     * @param auditorium_show the auditorium_show to set
     */
    public void setAuditorium_show(Auditorium auditorium_show) {
        this.auditorium_show = auditorium_show;
    }

    /**
     * @return the canRemove
     */
    public boolean isCanRemove() {
        return canRemove;
    }

    /**
     * @param canRemove the canRemove to set
     */
    public void setCanRemove(boolean canRemove) {
        this.canRemove = canRemove;
    }

    /**
     * @return the toRemove
     */
    public boolean isToRemove() {
        return toRemove;
    }

    /**
     * @param toRemove the toRemove to set
     */
    public void setToRemove(boolean toRemove) {
        this.toRemove = toRemove;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the whiteboard
     */
    public boolean isWhiteboard() {
        return whiteboard;
    }

    /**
     * @param whiteboard the whiteboard to set
     */
    public void setWhiteboard(boolean whiteboard) {
        this.whiteboard = whiteboard;
    }

    /**
     * @return the projector
     */
    public boolean isProjector() {
        return projector;
    }

    /**
     * @param projector the projector to set
     */
    public void setProjector(boolean projector) {
        this.projector = projector;
    }

    /**
     * @return the master_comp
     */
    public boolean isMaster_comp() {
        return master_comp;
    }

    /**
     * @param master_comp the master_comp to set
     */
    public void setMaster_comp(boolean master_comp) {
        this.master_comp = master_comp;
    }

    /**
     * @return the message2
     */
    public String getMessage2() {
        return message2;
    }

    /**
     * @param message2 the message2 to set
     */
    public void setMessage2(String message2) {
        this.message2 = message2;
    }

    /**
     * @return the capacity
     */
    public String getCapacity() {
        return capacity;
    }

    /**
     * @param capacity the capacity to set
     */
    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }
}
