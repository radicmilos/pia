/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import db.Admin;
import db.DbFactory;
import db.Student;
import db.Subject;
import db.SubjectTeacher;
import db.Teacher;
import db.User;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author radic
 */
@ManagedBean
@ViewScoped
public class Login implements Serializable{
    
    private User user;
    
    private String username;
    private String password;
    private String password_new;
    private int currentType = User.STUDENT_TYPE;
    private UploadedFile upFile;
    
    private String year;
    private String field;
    private String title;
    private String office;
    
    private List<Subject> allSubjects;
    private List<Subject> mySubjects;
    private List<String> nameSubjects;
    private List<String> nameSelected;
    private UIInput nameComp;
    private UIInput surnameComp;
    
    public String logout(){
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "index.xhtml?faces-redirect=true";
    }
    
    
    public boolean checkUsernameStudent(char nameF, char surnameF, String username)
    {
        if (username == null || username.length() != 9)
            return false;
        if (username.charAt(0) != surnameF || username.charAt(1) != nameF)
            return false;
        if (username.charAt(8) != 'd' && username.charAt(8) != 'm' && username.charAt(8) != 'p')
            return false;
        for (int i = 2; i < 8; ++i)
        {
            if (!Character.isDigit(username.charAt(i)))
                return false;
        }
        return true;
    }
    
    public void checkUsername(FacesContext context, UIComponent component, Object value)
    {
        String val = (String)value;
        if (currentType == User.STUDENT_TYPE)
        {
            String name = (String)nameComp.getLocalValue();
            String surname = (String)surnameComp.getLocalValue();
            if (name == null || surname == null || name.length() == 0 || surname.length() == 0 || !checkUsernameStudent(name.charAt(0), surname.charAt(0), val))
            {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Studentska sifra mora biti u formatu ipbbggggt", "Studentska sifra mora biti u formatu ipbbggggt"));
            }  
        }
        
    }
    
    public void checkPassword(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String val = (String)value;
        boolean hasUpper = false;
        boolean hasLower = false;
        boolean hasDigit = false;
        if (val.length() < 6 || val.length() > 12)
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Sifra mora biti duzine izmedju 6 i 12", "Sifra mora biti duzime izmedju 6 i 12"));
        if(!Character.isLetter(val.charAt(0)))
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Prvi karakter mora biti slovo", "Prvi karakter mora biti slovo"));
        for (Character c : val.toCharArray())
        {
            if (Character.isUpperCase(c))
                hasUpper = true;
            else if (Character.isLowerCase(c))
                hasLower = true;
            else if (Character.isDigit(c))
                hasDigit = true;
        }
        if (!hasDigit || !hasUpper || !hasLower)
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Mora postojati cifra, malo slovo i veliko slovo u sifri", "Mora postojati cifra, malo slovo i veliko slovo u sifri"));
        for (int i = 0; i < val.length() - 2; ++i)
        {
            if (val.charAt(i) == val.charAt(i + 1) && val.charAt(i) == val.charAt(i + 2))
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ne smeju biti ista 3 uzastopna karaktera", "Ne smeju biti ista 3 uzastopna karaktera"));
        }
    }
    
    /*
    public void newPic(FileUploadEvent event)
    {
        try {
            File file = File.createTempFile(event.getFile().getFileName(), ".tmp");
            FileOutputStream output = new FileOutputStream(file);
            InputStream input = event.getFile().getInputstream();
            byte[] buffer = new byte[10000];
            int bulk;
 while(true){
                bulk = input.read(buffer);
                if (bulk < 0){
                    break;
                }
                output.write(buffer,0,bulk);
                output.flush();
            }
            setUpFile(file);
        } catch (IOException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    */
    public String register()
    {
        /*
        if (!checkIdentificationNumber(user))
        {
            FacesContext.getCurrentInstance().addMessage("jsf_message", new FacesMessage("Nije validan maticni broj"));
            return null;
        }*/
        if (currentType == User.STUDENT_TYPE)
        {
            Student s = (Student)user;
            s.setYear(year);
            s.setField(field);
        }
        else if (currentType == User.TEACHER_TYPE)
        {
            Teacher t= (Teacher)user;
            t.setOffice(office);
            t.setTitle(title);
        }
            
        InputStream input = null;
        OutputStream output = null;
    try {
        ExternalContext extContext = FacesContext.getCurrentInstance().getExternalContext();
    input = upFile.getInputstream();
    File dest = new File(extContext.getRealPath("//resources//uploads//" + user.getUsername()));
    output = new FileOutputStream(dest);
        IOUtils.copy(input, output);
    }
    catch(Exception e) {
        e.printStackTrace();
    }
    finally {
        IOUtils.closeQuietly(input);
        IOUtils.closeQuietly(output);
    }
        user.setApproved(User.APPROVED_CHOOSE);
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(user);
        session.getTransaction().commit();
        session.close();   
        if (currentType == User.TEACHER_TYPE)
        {
            Teacher t = (Teacher)user;
                        Session session2 = DbFactory.getSessionFactory().openSession();
            session2.beginTransaction();
            for (Subject s : mySubjects)
            {
                SubjectTeacher st = new SubjectTeacher();
                st.setSubject(s);
                st.setTeacher(t);
                session2.save(st);
            }
            session2.getTransaction().commit();
            session2.close(); 
        }
        return "index.xhtml?faces-redirect=true";
    }
    
    public void userChanged()
    {
        setCurrentType(user.getType());
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        Query q = session.createQuery("From Subject");
        allSubjects = q.list();
        nameSubjects = new ArrayList<String> ();
        nameSelected = new ArrayList<String> ();
        for (Subject s : allSubjects)
        {
            nameSubjects.add(s.getName());
        }
        session.close();
        mySubjects = null;

    }
    public void newUser()
    {
        if (currentType == User.ADMIN_TYPE)
            user = new Admin();
        else if (currentType == User.STUDENT_TYPE)
            user = new Student();
        else if (currentType == User.TEACHER_TYPE)
            user = new Teacher();
        user.setType(getCurrentType());
    }
    
    public String newRegister()
    {
        return "register.xhtml?faces-redirect=true";
    }
    public String changePassword()
    {
        if (password.equals(password_new))
        {
            FacesContext.getCurrentInstance().addMessage("jsf_message", new FacesMessage("Nova sifra mora biti drugacija"));
            return null;
        }
        User user = getUser(username, password);
        if (user == null)
        {
            FacesContext.getCurrentInstance().addMessage("jsf_message", new FacesMessage("Nepostojeci korisnik"));
            return null; 
        }
        user.setPassword(password_new);
        Session session = DbFactory.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(user);
        session.getTransaction().commit();
        session.close();
        return "index.xhtml?faces-redirect=true";
    }
    
    private User getUser(String username, String password)
    {
        Session session = DbFactory.getSessionFactory().openSession();
        Query q;
        List<User> results = null;
      
        session.beginTransaction();
        
        q = session.createQuery("FROM Student AS u WHERE username=:u AND password=:p");
        q.setParameter("u", username);
        q.setParameter("p", password);
        results = q.list();
        if (results.size() > 0)
        {
            session.close();
            return (User)results.get(0);
        }
        q = session.createQuery("FROM Teacher AS u WHERE username=:u AND password=:p");
        q.setParameter("u", username);
        q.setParameter("p", password);
        results = q.list(); 
        if (results.size() > 0)
        {
            session.close();
            return (User)results.get(0);
        }
        q = session.createQuery("FROM Admin AS u WHERE username=:u AND password=:p");
        q.setParameter("u", username);
        q.setParameter("p", password);
        results = q.list(); 
        if (results.size() > 0)
        {
            session.close();
            return (User)results.get(0);
        }
        session.close();
        return null;
    }
    
    public String checkLogin() throws IOException {
        User user = getUser(username, password);
        if (user != null && user.getApproved() == User.APPROVED_YES)
            return loginSuccess(user);
        else
            return loginFailed();
    }
    
    private String loginSuccess(User user)
    {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
        if (user instanceof Student)
        {
            user.setType(User.STUDENT_TYPE);
            return "home.xhtml?faces-redirect=true";
        }
        if (user instanceof  Admin)
        {
            user.setType(User.ADMIN_TYPE);
            return "home.xhtml?faces-redirect=true";
        }
        if (user instanceof Teacher)
        {
            user.setType(User.TEACHER_TYPE);
            return "home.xhtml?faces-redirect=true";
        }
        return null;
    }
    private String loginFailed() {
        FacesContext.getCurrentInstance().addMessage("jsf_message", new FacesMessage("Nepostojeci korisnik"));
        return null;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the password_new
     */
    public String getPassword_new() {
        return password_new;
    }

    /**
     * @param password_new the password_new to set
     */
    public void setPassword_new(String password_new) {
        this.password_new = password_new;
    }

    /**
     * @return the year
     */
    public String getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(String year) {
        this.year = year;   }

    /**
     * @return the currentType
     */
    public int getCurrentType() {
        return currentType;
    }

    /**
     * @param currentType the currentType to set
     */
    public void setCurrentType(int currentType) {
        this.currentType = currentType;
;
    }

    /**
     * @return the upFile
     */
    public UploadedFile getUpFile() {
        return upFile;
    }

    /**
     * @param upFile the upFile to set
     */
    public void setUpFile(UploadedFile upFile )
    {
        this.upFile = upFile;
    }
    /**
     * @return the field
     */
    public String getField() {
        return field;
    }

    /**
     * @param field the field to set
     */
    public void setField(String field) {
        this.field = field;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the office
     */
    public String getOffice() {
        return office;
    }

    /**
     * @param office the office to set
     */
    public void setOffice(String office) {
        this.office = office;
    }

    /**
     * @return the nameComp
     */
    public UIInput getNameComp() {
        return nameComp;
    }

    /**
     * @param nameComp the nameComp to set
     */
    public void setNameComp(UIInput nameComp) {
        this.nameComp = nameComp;
    }

    /**
     * @return the surnameComp
     */
    public UIInput getSurnameComp() {
        return surnameComp;
    }

    /**
     * @param surnameComp the surnameComp to set
     */
    public void setSurnameComp(UIInput surnameComp) {
        this.surnameComp = surnameComp;
    }

    private static Map<String, Integer> citiesMap;

    static {
        citiesMap = new HashMap<String, Integer>();
        citiesMap.put("Beograd", 71);
        citiesMap.put("Zemun", 71);
        citiesMap.put("Obrenovac", 71);

        citiesMap.put("Kragujevac", 72);
        citiesMap.put("Aranđelovac", 72);
        citiesMap.put("Svilajnac", 72);
        citiesMap.put("Paraćin", 72);
        citiesMap.put("Jagodina", 72);
        citiesMap.put("Ćuprija", 72);

        citiesMap.put("Niš", 73);
        citiesMap.put("Aleksinac", 73);
        citiesMap.put("Pirot", 73);
        citiesMap.put("Prokuplje", 73);
        citiesMap.put("Kuršumlija", 73);

        citiesMap.put("Leskovac", 74);
        citiesMap.put("Vlasotince", 74);
        citiesMap.put("Vranje", 74);
        citiesMap.put("Vladičin Han", 74);

        citiesMap.put("Zaječar", 75);
        citiesMap.put("Boljevac", 75);
        citiesMap.put("Veliko Knjaževac", 75);
        citiesMap.put("Sokobanja", 75);
        citiesMap.put("Bor", 75);
        citiesMap.put("Majdanpek", 75);
        citiesMap.put("Kladovo", 75);
        citiesMap.put("Negotin", 75);

        citiesMap.put("Smederevska Palanka", 76);
        citiesMap.put("Velika Plana", 76);
        citiesMap.put("Smederevo", 76);
        citiesMap.put("Veliko Gradište", 76);
        citiesMap.put("Kučevo", 76);
        citiesMap.put("Petrovac na Mlavi", 76);
        citiesMap.put("Požarevac", 76);
        citiesMap.put("Žagubica", 76);
        citiesMap.put("Golubac", 76);

        citiesMap.put("Loznica", 77);
        citiesMap.put("Krupanj", 77);
        citiesMap.put("Ljubovija", 77);
        citiesMap.put("Šabac", 77);
        citiesMap.put("Bogatić", 77);
        citiesMap.put("Mali Zvornik", 77);
        citiesMap.put("Valjevo", 77);
        citiesMap.put("Lajkovac", 77);
        citiesMap.put("Ljig", 77);
        citiesMap.put("Ub", 77);
        citiesMap.put("Osečina", 77);
        citiesMap.put("Mionoca", 77);

        citiesMap.put("Kraljevo", 78);
        citiesMap.put("Vrnjačka Banja", 78);
        citiesMap.put("Novi Pazar", 78);
        citiesMap.put("Raška", 78);
        citiesMap.put("Tutin", 78);
        citiesMap.put("Čačak", 78);
        citiesMap.put("Ivanjica", 78);
        citiesMap.put("Lučani", 78);
        citiesMap.put("Aleksandrovac", 78);
        citiesMap.put("Brus", 78);
        citiesMap.put("Kruševac", 78);
        citiesMap.put("Trstenik", 78);
        citiesMap.put("Varvarin", 78);
        citiesMap.put("Ćićevac", 78);

        citiesMap.put("Užice", 79);
        citiesMap.put("Arilje", 79);
        citiesMap.put("Bajina Bašta", 79);
        citiesMap.put("Kosjerić", 79);
        citiesMap.put("Požega", 79);
        citiesMap.put("Priboj", 79);
        citiesMap.put("Prijepolje", 79);
        citiesMap.put("Sjenica", 79);
        citiesMap.put("Čajetina", 79);
        citiesMap.put("Nova Varoš", 79);

        citiesMap.put("Novi Sad", 80);
        citiesMap.put("Bač", 80);
        citiesMap.put("Bačka Palanka", 80);
        citiesMap.put("Vrbas", 80);
        citiesMap.put("Sremski Karlovci", 80);
        citiesMap.put("Temerin", 80);
        citiesMap.put("Titel", 80);
        citiesMap.put("Bečej", 80);
        citiesMap.put("Beočin", 80);

        citiesMap.put("Apatin", 81);
        citiesMap.put("Kula", 81);
        citiesMap.put("Sombor", 81);

        citiesMap.put("Subotica", 82);
        citiesMap.put("Mali Iđoš", 82);

        citiesMap.put("Zrenjanin", 85);
        citiesMap.put("Novi Bečej", 85);
        citiesMap.put("Žitište", 85);

        citiesMap.put("Vršac", 86);
        citiesMap.put("Kovin", 86);
        citiesMap.put("Pančevo", 86);
        citiesMap.put("Opovo", 86);

        citiesMap.put("Ada", 87);
        citiesMap.put("Kikinda", 87);
        citiesMap.put("Kanjiža", 87);
        citiesMap.put("Senta", 87);
        citiesMap.put("Čoka", 87);

        citiesMap.put("Inđija", 88);
        citiesMap.put("Ruma", 88);
        citiesMap.put("Sremska Mitrovica", 88);
        citiesMap.put("Stara Pazova", 88);
        citiesMap.put("Šid", 88);
    }
    
    private static final Pattern paternIdentificationNumber = Pattern.compile("^[0-9]{13}$");
    private boolean checkIdentificationNumber(User user) {
        String identificationNumber = user.getJmbg();

        //length
        Matcher matcher = paternIdentificationNumber.matcher(identificationNumber);
        if (!matcher.matches()) {
            return false;
        }

        //birth
        if (!isValidBirthDate(identificationNumber.substring(0, 7))) {
            return false;
        }

        //region
        if (!"/".equals(user.getBirthplace())) {
            int regionCode = Integer.parseInt(identificationNumber.substring(7, 9));
            if (citiesMap.get(user.getBirthplace()) != null && regionCode != citiesMap.get(user.getBirthplace())) { 
                return false;
            }
        }

        //gender
        int uniqueNumber = Integer.parseInt(identificationNumber.substring(9, 12));
            if (user.getGender().equals("M")) 
            {
                if (uniqueNumber >= 500) {
                    return false;
                }
            }
            if (user.getGender().equals("Z")) 
            {
                if (uniqueNumber < 500) {
                    return false;
                }
            }
        

        //control chek sum
        int chekSum = identificationNumber.charAt(12) - 48;
        if (chekSum != calculateIdentificationNumberCheckSum(identificationNumber)) {
            return false;
        }
        
        return true;
    }
    
    
    
        private static int calculateIdentificationNumberCheckSum(String identificationNumber) {
        int ch00 = identificationNumber.charAt(0) - 48;
        int ch01 = identificationNumber.charAt(1) - 48;
        int ch02 = identificationNumber.charAt(2) - 48;
        int ch03 = identificationNumber.charAt(3) - 48;
        int ch04 = identificationNumber.charAt(4) - 48;
        int ch05 = identificationNumber.charAt(5) - 48;
        int ch06 = identificationNumber.charAt(6) - 48;
        int ch07 = identificationNumber.charAt(7) - 48;
        int ch08 = identificationNumber.charAt(8) - 48;
        int ch09 = identificationNumber.charAt(9) - 48;
        int ch10 = identificationNumber.charAt(10) - 48;
        int ch11 = identificationNumber.charAt(11) - 48;

        int checkSum =11 - ((7 * (ch00 + ch06) + 6 * (ch01 + ch07) + 5 * (ch02 + ch08) + 4 * (ch03 + ch09) + 3 * (ch04 + ch10) + 2 * (ch05 + ch11)) % 11);
        if (checkSum>9) return 0;
        return checkSum;
    }
        
        private static boolean isValidBirthDate(String dateToValidate) {
        Date date;
        Date today = new Date();
        Date oldestBirth = new Date();
        oldestBirth.setYear(today.getYear() - 120);

        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        dateFormat.setLenient(false);

        String dateBefore2000;
        String dateAfter2000;

        try {
            dateBefore2000 = dateToValidate.substring(0, 4) + "1" + dateToValidate.substring(4, 7);
            dateAfter2000 = dateToValidate.substring(0, 4) + "2" + dateToValidate.substring(4, 7);
        } catch (StringIndexOutOfBoundsException ex) {
            return false;
        }

        try {
            date = dateFormat.parse(dateAfter2000);

            if (date.after(today) || date.before(oldestBirth)) {
                try {
                    date = dateFormat.parse(dateBefore2000);
                    if (date.after(today) || date.before(oldestBirth)) {
                        return false;
                    }
                } catch (ParseException ex) {
                    return false;
                }
            }
        } catch (ParseException ex) {
            return false;
        }
        return true;
    }

    /**
     * @return the allSubjects
     */
    public List<Subject> getAllSubjects() {
        return allSubjects;
    }

    /**
     * @param allSubjects the allSubjects to set
     */
    public void setAllSubjects(List<Subject> allSubjects) {
        this.allSubjects = allSubjects;
    }

    /**
     * @return the mySubjects
     */
    public List<Subject> getMySubjects() {
        return mySubjects;
    }

    /**
     * @param mySubjects the mySubjects to set
     */
    public void setMySubjects(List<Subject> mySubjects) {
        this.mySubjects = mySubjects;
    }

    /**
     * @return the nameSubjects
     */
    public List<String> getNameSubjects() {
        return nameSubjects;
    }

    /**
     * @param nameSubjects the nameSubjects to set
     */
    public void setNameSubjects(List<String> nameSubjects) {
        this.nameSubjects = nameSubjects;
    }

    /**
     * @return the nameSelected
     */
    public List<String> getNameSelected() {
        return nameSelected;
    }

    /**
     * @param nameSelected the nameSelected to set
     */
    public void setNameSelected(List<String> nameSelected) {
        this.nameSelected = nameSelected;
        mySubjects = new ArrayList<Subject> ();
        for (String s : nameSelected)
        {
            for (Subject a : allSubjects)
            {
                if (a.getName().equals(s))
                {
                    mySubjects.add(a);
                    break;
                }
            }
        }
    }

}
