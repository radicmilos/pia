-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 17, 2016 at 08:27 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `reservation_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `auditorium`
--

CREATE TABLE IF NOT EXISTS `auditorium` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `capacity` int(11) NOT NULL,
  `whiteboard` bit(1) NOT NULL,
  `master_comp` bit(1) NOT NULL,
  `projector` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `auditorium`
--

INSERT INTO `auditorium` (`id`, `name`, `capacity`, `whiteboard`, `master_comp`, `projector`) VALUES
(10, '25', 30, b'1', b'1', b'1'),
(11, '26', 35, b'1', b'1', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE IF NOT EXISTS `reservation` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `teacher` int(11) NOT NULL,
  `date` date NOT NULL,
  `startHour` int(11) NOT NULL,
  `endHour` int(11) NOT NULL,
  `startMinute` int(11) NOT NULL,
  `endMinute` int(11) NOT NULL,
  `used` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `type` varchar(40) NOT NULL,
  `description` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`id`, `teacher`, `date`, `startHour`, `endHour`, `startMinute`, `endMinute`, `used`, `year`, `type`, `description`) VALUES
(21, 25, '2016-02-19', 1, 3, 0, 0, 0, 4, 'SI', 'PIA-odbrane'),
(22, 25, '2016-02-19', 1, 3, 30, 30, 0, 4, 'SI', 'PIA-odbrane'),
(23, 25, '2016-02-20', 2, 4, 0, 0, 0, 3, 'SI', 'PIA-rekapitulacija'),
(24, 25, '2016-02-15', 1, 3, 0, 0, 0, 3, 'RTI', 'nesto');

-- --------------------------------------------------------

--
-- Table structure for table `reservationstudent`
--

CREATE TABLE IF NOT EXISTS `reservationstudent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservation` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `reservationstudent`
--

INSERT INTO `reservationstudent` (`id`, `reservation`, `student`) VALUES
(14, 22, 24);

-- --------------------------------------------------------

--
-- Table structure for table `reservation_auditorium`
--

CREATE TABLE IF NOT EXISTS `reservation_auditorium` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservation` int(11) NOT NULL,
  `auditorium` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ra_1` (`auditorium`),
  KEY `ra_2` (`reservation`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `reservation_auditorium`
--

INSERT INTO `reservation_auditorium` (`id`, `reservation`, `auditorium`) VALUES
(22, 21, 10),
(23, 22, 11),
(24, 23, 10),
(25, 24, 10);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `id` int(11) NOT NULL,
  `year` varchar(30) NOT NULL,
  `field` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `year`, `field`) VALUES
(24, '4', 'SI');

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE IF NOT EXISTS `subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`id`, `name`) VALUES
(1, 'matematika'),
(2, 'programiranje');

-- --------------------------------------------------------

--
-- Table structure for table `subject_teacher`
--

CREATE TABLE IF NOT EXISTS `subject_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` int(11) NOT NULL,
  `teacher` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `subject_teacher`
--

INSERT INTO `subject_teacher` (`id`, `subject`, `teacher`) VALUES
(3, 2, 25);

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE IF NOT EXISTS `teacher` (
  `id` int(11) NOT NULL,
  `office` varchar(20) NOT NULL,
  `title` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`id`, `office`, `title`) VALUES
(25, '37', 'Predavac');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `name` varchar(40) NOT NULL,
  `surname` varchar(40) NOT NULL,
  `gender` varchar(5) NOT NULL,
  `birthplace` varchar(40) NOT NULL,
  `jmbg` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `approved` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `name`, `surname`, `gender`, `birthplace`, `jmbg`, `phone`, `email`, `approved`, `type`) VALUES
(1, 'admin', 'aAA123', 'admin1', 'adminic', 'M', 'Beograd', '1231', '062 621 1711', 'admin@etf.rs', 2, 2),
(24, 'rm120010d', 'Radic123', 'milos', 'radic', 'M', 'Beograd', '124142', '0643515881', 'radic_milos@hotmail.com', 2, 0),
(25, 'drazen', 'Drazen123', 'drazen', 'draskovic', 'M', 'Beograd', '12415', '12515', 'drazen@etf.rs', 2, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user` (`id`);

--
-- Constraints for table `reservation_auditorium`
--
ALTER TABLE `reservation_auditorium`
  ADD CONSTRAINT `ra_1` FOREIGN KEY (`auditorium`) REFERENCES `auditorium` (`id`),
  ADD CONSTRAINT `ra_2` FOREIGN KEY (`reservation`) REFERENCES `reservation` (`id`);

--
-- Constraints for table `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `student_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user` (`id`);

--
-- Constraints for table `teacher`
--
ALTER TABLE `teacher`
  ADD CONSTRAINT `teacher_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
